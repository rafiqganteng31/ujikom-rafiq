<!DOCTYPE html>
<?php
include "koneksi.php";
$id=$_GET['id'];

$select=mysqli_query($koneksi,"select * from user where id='$id'");
$data=mysqli_fetch_array($select);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Peminjaman </title>

    <!-- Bootstrap core CSS -->

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/maps/jquery-jvectormap-2.0.1.css" />
    <link href="css/icheck/flat/green.css" rel="stylesheet" />
    <link href="css/floatexamples.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery.min.js"></script>
    <script src="js/nprogress.js"></script>
    <script>
        NProgress.start();
    </script>
    
    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>


<body class="nav-md">

    <div class="container body">


        <div class="main_container">

            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">

                    <div class="navbar nav_title" style="border: 0;">
                        <a href="#" class="site_title"><i class="fa fa-paw"></i> <span>Peminjaman</span></a>
                    </div>
                    <div class="clearfix"></div>

                    <!-- menu prile quick info -->
                   
                    <!-- /menu prile quick info -->

                    <br />

                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <ul class="nav side-menu">
								<li><a href="peralatan.php"><i class="fa fa-desktop"></i> Peralatan</a></li>
								<li><a href="tambah_peralatan.php"><i class="fa fa-desktop"></i> Tambah Peralatan</a></li>
                                <li><a><i class="fa fa-edit"></i> Peminjaman <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                       
                                        <li><a href="data_peminjaman.php">Data Peminjaman</a>
                                        </li>
                                        <li><a href="tambah_peminjaman.php">Tambah Peminjaman</a>
                                    </ul>
                                </li>
								<li><a href="pengembalian.php"><i class="fa fa-desktop"></i> Pengembalian</a></li> 
                             </ul>
                        </div>
                    </div>
                    <!-- /sidebar menu -->
				
			</div>
		</div>
	</div>
	<!-- start: Header -->
	
		<div class="container-fluid-full">
		<div class="row-fluid">
				
			<!-- start: Main Menu -->
			<div id="sidebar-left" class="span2">
				<div class="nav-collapse sidebar-nav">
					<ul class="nav nav-tabs nav-stacked main-menu">
						<li><a href="calendar.html"><i class="icon-calendar"></i><span class="hidden-tablet"> PERALATAN</span></a></li>.
						<li><a href="icon.html"><i class="icon-calendar"></i><span class="hidden-tablet"> PEMINJAMAN</span></a></li>
						<li><a href="login.html"><i class="icon-calendar"></i><span class="hidden-tablet"> PENGEMBALIAN</span></a></li>
					</ul>
				</div>
			</div>
			<!-- end: Main Menu -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<!-- start: Content -->
			<div id="content" class="span10">
					<ul class="breadcrumb">
						<li>
							<i class="icon-home"></i>
							<a href="#.html">Home</a> 
							<i class="icon-angle-right"></i>
						</li>
						<li><a href="#">Dashboard</a></li>
					</ul>
						
							<div class="row-fluid sortable">
								<div class="box span12">
									<div class="box-header" data-original-title>
										<h2><i class="halflings-icon white edit"></i><span class="break"></span>Form Elements</h2>
										<div class="box-icon">
											<a href="#" class="btn-setting"><i class="halflings-icon white wrench"></i></a>
											<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
											<a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
										</div>
									</div>
									<div class="box-content">
										<form  action="update_peralatan.php?id=<?php echo $id; ?>" method="post" class="form-horizontal">
											<fieldset>
											  <div class="control-group">
												<label class="control-label">Nama Siswa</label>
												<div class="controls">
												  <input name ="nama_siswa" class="form-control" value="<?php echo $data['nama_siswa'];?>" type="text">
												</div>
											  </div>
											  
											  <div class="control-group">
												<label class="control-label">Nama Barang</label>
												<div class="controls">
												  <input name ="nama_barang" class="form-control" value="<?php echo $data['nama_barang'];?>" type="text">
												</div>
											  </div>
											  <div class="control-group">
												<label class="control-label">Kelas</label>
												<div class="controls">
												  <input name ="kelas" class="form-control" value="<?php echo $data['kelas'];?>" type="text">
												</div>
											  </div>
											  
											</fieldset>
											<button type="submit" class="btn btn-primary">Update</button>
										</form>
											
									</div><!--/span-->
	                            </div>
							</div>
			</div>
	<div class="common-modal modal fade" id="common-Modal1" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-content">
			<ul class="list-inline item-details">
				<li><a href="http://themifycloud.com">Admin templates</a></li>
				<li><a href="http://themescloud.org">Bootstrap themes</a></li>
			</ul>
		</div>
	</div>
	
	<div class="clearfix"></div>
	
	<footer>

		<p>
			<span style="text-align:left;float:left">&copy; 2013 <a href="http://themifycloud.com/downloads/janux-free-responsive-admin-dashboard-template/" alt="Bootstrap_Metro_Dashboard"></a></span>
			
		</p>

	</footer>
	
	<!-- start: JavaScript-->

		<script src="js/jquery-1.9.1.min.js"></script>
	<script src="js/jquery-migrate-1.0.0.min.js"></script>
	
		<script src="js/jquery-ui-1.10.0.custom.min.js"></script>
	
		<script src="js/jquery.ui.touch-punch.js"></script>
	
		<script src="js/modernizr.js"></script>
	
		<script src="js/bootstrap.min.js"></script>
	
		<script src="js/jquery.cookie.js"></script>
	
		<script src='js/fullcalendar.min.js'></script>
	
		<script src='js/jquery.dataTables.min.js'></script>

		<script src="js/excanvas.js"></script>
	<script src="js/jquery.flot.js"></script>
	<script src="js/jquery.flot.pie.js"></script>
	<script src="js/jquery.flot.stack.js"></script>
	<script src="js/jquery.flot.resize.min.js"></script>
	
		<script src="js/jquery.chosen.min.js"></script>
	
		<script src="js/jquery.uniform.min.js"></script>
		
		<script src="js/jquery.cleditor.min.js"></script>
	
		<script src="js/jquery.noty.js"></script>
	
		<script src="js/jquery.elfinder.min.js"></script>
	
		<script src="js/jquery.raty.min.js"></script>
	
		<script src="js/jquery.iphone.toggle.js"></script>
	
		<script src="js/jquery.uploadify-3.1.min.js"></script>
	
		<script src="js/jquery.gritter.min.js"></script>
	
		<script src="js/jquery.imagesloaded.js"></script>
	
		<script src="js/jquery.masonry.min.js"></script>
	
		<script src="js/jquery.knob.modified.js"></script>
	
		<script src="js/jquery.sparkline.min.js"></script>
	
		<script src="js/counter.js"></script>
	
		<script src="js/retina.js"></script>

		<script src="js/custom.js"></script>
	<!-- end: JavaScript-->
	
</body>
</html>
