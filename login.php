<?php
    include"koneksi.php";
        
    if(isset($_username)){
    header("location:index.php");
}else{

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  
    <title>Login</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/login.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <link rel="icon" href="img/favicon.ico">
  </head>
  <body class="cyan" background="img/test.jpg">
  <div class="container-fluid">
  <div class="row">
  <div class="login-form">
    <div class="log col-md-4 col-md-offset-4">
      <div class="panel panel-default animated slideInDown top50">
        <div class="panel-body">
          <h2>Login</h2>
          <form action="proses_login.php" method="POST">
          <div class="input-group input-custom">
            <span class="input-group-addon" id="basic-addon1"><i class="fa fa-user-circle"></i></span>
            <input type="text" name="username" class="form-control" placeholder="Username" aria-describedby="basic-addon1">
          </div>
          <div class="input-group input-custom">
            <span class="input-group-addon" id="basic-addon1"><i class="fa fa-lock"></i></span>
            <input type="password" name="password" class="form-control" placeholder="Password" aria-describedby="basic-addon1">
          </div>  
          <div class="checkbox">
            <label>
              <input type="checkbox">Login</label>
          </div>
          <button type="submit" class="btn btn-info active btn-lg btn-block">LOGIN</button>
      </form>
        </div>
      </div>
    </div> 
  </div>
  </div>
  </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
<?php
}
?>